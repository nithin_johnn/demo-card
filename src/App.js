import React from 'react';
import './like.png';



const Card = ({name, email ,username, id,pic}) => {
    
    return (
        
      <div className='tc bg-light-blue dib br3 pa3 ma2 grow bw2 shadow-5 baloon'>
      
        
          <div >
         
           <h2> {name} </h2>
            <p>{email}</p>
             <p>{username}</p>
            <p> {pic}</p>
          
            <img src="like.png" alt="image"/>
          </div>
        </div>
        
    );
}



export default Card;
