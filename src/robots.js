export const robots = [
  {
    id: 1,
    name: 'Leanne Graham',
    username: 'Bret',
    email: 'Sincere@april.biz',
    pic: 'Learn React? Start Small'
  },
  {
    id: 2,
    name: 'Ervin Howell',
    username: 'Antonette',
    email: 'Shanna@melissa.tv',
      pic: 'Learn React? Start Small'
  },
  {
    id: 3,
    name: 'Clementine Bauch',
    username: 'Samantha',
    email: 'Nathan@yesenia.net',
      pic: 'Learn React? Start Small'
  },
  {
    id: 4,
    name: 'Patricia Lebsack',
    username: 'Karianne',
    email: 'Julianne.OConner@kory.org',
      pic: 'Learn React? Start Small'
  },
  {
    id: 5,
    name: 'Chelsey Dietrich',
    username: 'Kamren',
    email: 'Lucio_Hettinger@annie.ca',
      pic: 'Learn React? Start Small'
  },
  {
    id: 6,
    name: 'Mrs. Dennis Schulist',
    username: 'Leopoldo_Corkery',
    email: 'Karley_Dach@jasper.info',
      pic: 'Learn React? Start Small'
  },
  {
    id: 7,
    name: 'Kurtis Weissnat',
    username: 'Elwyn.Skiles',
    email: 'Telly.Hoeger@billy.biz',
      pic: 'Learn React? Start Small'
  },
  {
    id: 8,
    name: 'Nicholas Runolfsdottir V',
    username: 'Maxime_Nienow',
    email: 'Sherwood@rosamond.me',
      pic: 'Learn React? Start Small'
  },
//  {
//    id: 9,
//    name: 'Glenna Reichert',
//    username: 'Delphine',
//    email: 'Chaim_McDermott@dana.io'
//  },
//  {
//    id: 10,
//    name: 'Clementina DuBuque',
//    username: 'Moriah.Stanton',
//    email: 'Rey.Padberg@karina.biz'
//  }
];
