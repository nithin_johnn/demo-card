import React from 'react';
import App from './App';

const CardList = ({ robots }) => {
    const cardComponent= robots.map((user, i)=>{
        return <App key={i} 
        id={robots[i].id} 
        name={robots[i].name} 
        email={robots[i].email} 
        username={robots[i].username} 
        pic={robots[i].pic}/>
    })
    return (
    <div>
    {cardComponent }
  
    </div>
    )
}

export default CardList;